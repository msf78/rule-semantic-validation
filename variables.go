package main

var Variables = VariablesSpec{
	"check.output.status": {
		Type:         String,
		Validation:   `{"in": [{"var": "this"}, ["enabled", "disabled"]]}`,
		ErrorMessage: "Error",
	},
}
