package main

type VariablesSpec map[string]VariableSpec

type VariableSpec struct {
	Type         VariableType
	Validation   string
	ErrorMessage string
}
