package main

import (
	"encoding/json"
	"errors"
	"strings"
)

var (
	ErrExpressionMustBeMap   = errors.New("This expression must be a map")
	ErrExpressionMustBeSlice = errors.New("This expression must be a slice")
)

func IsValid(ruleCondition string) (bool, error) {
	reader := strings.NewReader(ruleCondition)
	decoder := json.NewDecoder(reader)
	var _ruleCondition interface{}

	err := decoder.Decode(&_ruleCondition)
	if err != nil {
		return false, err
	}

	return validateExpression(_ruleCondition, nil)
}

func validateExpression(ruleCondition interface{}, parent *Expression) (bool, error) {
	if isLiteral(ruleCondition) {
    // TODO: Add variable validations. 
		return true, nil
	}

	if !isMap(ruleCondition) {
		return false, ErrExpressionMustBeMap
	}

	expression := FromMap(ruleCondition.(map[string]interface{}))

	if !isSlice(expression.Operands) {
		return false, ErrExpressionMustBeSlice
	}


	for _, operand := range expression.Operands {
		valid, err := validateExpression(operand, expression)
		if !valid {
			return false, err
		}
	}

	return true, nil
}
