package main

import (
	"reflect"
)

func isMap(rule interface{}) bool {
	return is(rule, reflect.Map)
}

func isSlice(rule interface{}) bool {
  return is(rule, reflect.Slice)
}

func isLiteral(rule interface{}) bool {
	return isBool(rule) || isString(rule) || isNumber(rule)
}

func isBool(rule interface{}) bool {
	return is(rule, reflect.Bool)
}

func isString(rule interface{}) bool {
	return is(rule, reflect.String)
}

func isNumber(rule interface{}) bool {
	return is(rule, reflect.Float64)
}

func is(rule interface{}, kind reflect.Kind) bool {
	return rule != nil && reflect.TypeOf(rule).Kind() == kind
}
