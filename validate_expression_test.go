package main

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestIsValid_Success(t *testing.T) {
	assert := require.New(t)

	valid, err := IsValid(`{">": [4, 5]}`)

	assert.True(valid)
	assert.Nil(err)
}

func TestIsValid_Fail(t *testing.T) {
	assert := require.New(t)

	valid, err := IsValid(`{">": {}}`)

	assert.False(valid)
	assert.Equal(ErrExpressionMustBeSlice, err)
}
