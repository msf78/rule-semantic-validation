package main

type VariableType string

const (
	String VariableType = "string"
	Number VariableType = "number"
)
