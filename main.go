package main

import (
	"fmt"
	"regexp"
)

func main() {
	fmt.Println("Hello, world")

	ruleIsValid, _ := IsValid(`{">": [4, 5]}`)
	fmt.Println("ruleIsValid: ", ruleIsValid)
}

func GetVariableNames(ruleCondition string) []string {
	varsRegex := regexp.MustCompile(`"var"\s*:\s*"([a-zA-Z._]+)"`)
	dedupedVars := make([]string, 0)
	dedupedAux := make(map[string]bool)

	matches := varsRegex.FindAllStringSubmatch(ruleCondition, -1)

	for i := 0; i < len(matches); i++ {
		for j := 1; j < len(matches[i]); j++ {
			if dedupedAux[matches[i][j]] {
				continue
			}

			dedupedVars = append(dedupedVars, matches[i][j])
			dedupedAux[matches[i][j]] = true
		}
	}

	return dedupedVars
}
