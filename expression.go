package main

type Expression struct {
	Operator  string
	Operands []interface{}
}

func NewExpression(operand string, operands []interface{}) *Expression {
	return &Expression{operand, operands}
}

func FromMap(expression map[string]interface{}) *Expression {
	var operator string = ""
	var operands []interface{}

	for key, value := range expression {
		operator = key
		operands = value.([]interface{})
		break
	}

	return NewExpression(operator, operands)
}
